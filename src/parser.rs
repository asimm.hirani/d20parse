use std::{str::Chars, iter::Peekable, collections::VecDeque};


#[derive(PartialEq, Eq, Debug, Clone, Copy)]
pub enum Grouping {
    OpenParenthesis,
    CloseParenthesis,
    OpenBracket,
    CloseBracket,
    OpenBrace,
    CloseBrace,
}

impl Grouping {
    pub fn is_open_grouping(&self) -> bool {
        use Grouping::*;
        matches!(self, OpenBrace | OpenParenthesis | OpenBracket)
    }

    /// If self is an opening group, see if other is our matching half, else false
    pub fn matches(&self, other : &Grouping) -> bool {
        use Grouping::*;

        let closer = match self {
            OpenParenthesis => CloseParenthesis,
            OpenBrace => CloseBrace,
            OpenBracket => CloseBracket,
            _ => return false,
        };

        *other == closer
    }
}

#[derive(PartialEq, Eq, Debug, Clone, Copy)]
pub enum RPToken {
    Numeric(i32),
    Product,
    Quotient,
    Sum,
    Difference,
    DiceRollOperator,
    GroupingToken(Grouping),
    KeepHighest,
    KeepLowest,
}

impl RPToken {

    pub fn is_operator(&self) -> bool {
        use RPToken::*;
        matches!(self, 
            Product |
            Quotient |
            Sum |
            Difference |
            DiceRollOperator |
            KeepHighest |
            KeepLowest
        )
    }

    pub fn get_operator_precedence(&self) -> i32 {
        match self {
            RPToken::Numeric(_) => 0,
            RPToken::Product => 2,
            RPToken::Quotient => 2,
            RPToken::Sum => 1,
            RPToken::Difference => 1,
            RPToken::DiceRollOperator => 3,
            RPToken::GroupingToken(_) => 0,
            RPToken::KeepHighest => 4,
            RPToken::KeepLowest => 4,
        }
    }

}

#[derive(PartialEq, Eq, Debug)]
pub enum ParseError {
    InvalidCharacter,
    EndOfExpression,
    UnmatchedGrouping
}

pub struct RollTokenizer<'a> {
    current_idx : Peekable<Chars<'a>>,    
}

impl<'a> RollTokenizer<'a> {
    pub fn new(in_str : &'a str) -> RollTokenizer<'a> {
        RollTokenizer { current_idx: in_str.chars().peekable() }
    }

    pub fn next(&mut self) -> Result<RPToken, ParseError> {
        use RPToken::*;
        if let Some(c) = self.current_idx.next() {
            match c {
                _ if c.is_ascii_digit() => {
                    let mut accumulator : String = String::new();
                    accumulator.push(c);
                    while let Some(peeked) = self.current_idx.peek() {
                        match peeked {
                            _ if peeked.is_ascii_digit() => {
                                accumulator.push(*peeked);
                                self.current_idx.next();
                                continue;
                            }
                            _ => break
                        }
                    }
                    Ok(Numeric(accumulator.parse().unwrap()))
                },
                'd' => Ok(DiceRollOperator),
                '(' => Ok(GroupingToken(Grouping::OpenParenthesis)),
                ')' => Ok(GroupingToken(Grouping::CloseParenthesis)),
                '{' => Ok(GroupingToken(Grouping::OpenBrace)),
                '}' => Ok(GroupingToken(Grouping::CloseBrace)),
                '[' => Ok(GroupingToken(Grouping::OpenBracket)),
                ']' => Ok(GroupingToken(Grouping::CloseBracket)),
                'k' => {
                    match self.current_idx.next() {
                        Some('h') => Ok(KeepHighest),
                        Some('l') => Ok(KeepLowest),
                        _ => Err(ParseError::InvalidCharacter)
                    }
                },
                '*' => Ok(Product),
                '+' => Ok(Sum),
                '/' => Ok(Quotient),
                '-' => Ok(Difference),
                _ if c.is_whitespace() => {
                    while let Some(peek) = self.current_idx.peek() {
                        if peek.is_whitespace() { self.current_idx.next(); }
                        else {
                            break;
                        }
                    }
                    self.next()
                }
                _ => Err(ParseError::InvalidCharacter)
            }
        } else {
            Err(ParseError::EndOfExpression)
        }

    }

}



/// Parse the tokens into RPN according to the shunting yard algorithm
pub fn parse(mut tokenizer : RollTokenizer) -> Result<Vec<RPToken>, ParseError> {
    let mut output_queue = VecDeque::new();
    let mut operator_stack = VecDeque::new();
    loop {
        match tokenizer.next() {
            Ok(token) => {
                match token {
                    RPToken::Numeric(_) => output_queue.push_back(token),
                    RPToken::GroupingToken(grouping) => {
                        if operator_stack.is_empty() && !grouping.is_open_grouping() {
                            return Err(ParseError::UnmatchedGrouping);
                        }
                        if operator_stack.is_empty() || grouping.is_open_grouping() {
                            operator_stack.push_back(token);
                        } else {
                            loop {
                                if let Some(top_operator) = operator_stack.pop_back() {
                                    match top_operator {
                                        RPToken::GroupingToken(top_grouping) if top_grouping.matches(&grouping) => break,
                                        RPToken::GroupingToken(_) => return Err(ParseError::UnmatchedGrouping),
                                        _ => output_queue.push_back(top_operator),
                                    }
                                } else {
                                    return Err(ParseError::UnmatchedGrouping);
                                }
                            }
                        }
                    },
                    _ if token.is_operator() => {
                        if operator_stack.is_empty() {
                            operator_stack.push_back(token);
                        } else {
                            while let Some(top_operator) = operator_stack.back() {
                                if let RPToken::GroupingToken(_) = top_operator {
                                    break;
                                } else if top_operator.get_operator_precedence() >= token.get_operator_precedence() {
                                    let top = operator_stack.pop_back().unwrap();
                                    output_queue.push_back(top);
                                } else {
                                    break;
                                }
                            }
                            operator_stack.push_back(token);
                        }
                    }
                    _ => unreachable!()
                }
            },
            Err(e) if e == ParseError::EndOfExpression => {
                break;
            },
            Err(e) => return Err(e)
        }
    }

    // Unload the operator stack into the result stack, err if we run into any groupings
    while let Some(token) = operator_stack.pop_back() {
        if let RPToken::GroupingToken(_) = token {
            return Err(ParseError::UnmatchedGrouping)
        }
        output_queue.push_back(token);
    }

    // Output stack should be in RPN now

    Ok(output_queue.into())
}




#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_basic_parsing() {
        let test_str = "[]";
        let mut parser = RollTokenizer::new(test_str);
        assert_eq!(Ok(RPToken::GroupingToken(Grouping::OpenBracket)), parser.next());
        assert_eq!(Ok(RPToken::GroupingToken(Grouping::CloseBracket)), parser.next());
        assert_eq!(Err(ParseError::EndOfExpression), parser.next());
    }

    #[test]
    fn test_numeric_parsing() {
        let test_str = "[450]"; 
        let mut parser = RollTokenizer::new(test_str);
        assert_eq!(Ok(RPToken::GroupingToken(Grouping::OpenBracket)), parser.next());
        assert_eq!(Ok(RPToken::Numeric(450)), parser.next());
        assert_eq!(Ok(RPToken::GroupingToken(Grouping::CloseBracket)), parser.next());
        assert_eq!(Err(ParseError::EndOfExpression), parser.next());
    }

    #[test]
    fn typical_dice_roll() {
        use RPToken::*;
        let test_str = "4d12kh2 + 5d8 + 2"; 
        let mut parser = RollTokenizer::new(test_str);
        let answer = vec![
            Numeric(4),
            DiceRollOperator,
            Numeric(12),
            KeepHighest,
            Numeric(2),
            Sum,
            Numeric(5),
            DiceRollOperator,
            Numeric(8),
            Sum,
            Numeric(2),
        ];
        let mut iter = answer.iter();
        while let (Ok(token), Some(answer)) = (parser.next(), iter.next()) {
            assert_eq!(*answer, token);
        }
        assert_eq!(Err(ParseError::EndOfExpression), parser.next());
    }

    #[test]
    fn test_simple_parsing() {
        let expr = "3d8";
        let tokenizer = RollTokenizer::new(expr);
        let parsed = parse(tokenizer).unwrap();

        let answer = vec![
            RPToken::Numeric(3),
            RPToken::Numeric(8),
            RPToken::DiceRollOperator,
        ];
        for (idx, tok) in parsed.iter().enumerate() {
            assert_eq!(answer[idx], *tok);
        }

    }

    #[test]
    fn test_good_grouping() {
        let expr = "3+2*(3/[5+3])";
        let tokenizer = RollTokenizer::new(expr);
        let parsed = parse(tokenizer).unwrap();

        let answer = vec![
            RPToken::Numeric(3),
            RPToken::Numeric(2),
            RPToken::Numeric(3),
            RPToken::Numeric(5),
            RPToken::Numeric(3),
            RPToken::Sum,
            RPToken::Quotient,
            RPToken::Product,
            RPToken::Sum
        ];
        for (idx, tok) in parsed.iter().enumerate() {
            assert_eq!(answer[idx], *tok);
        }

    }

    #[test]
    fn test_bad_grouping() {
        let expr = "3+2*(3/[5+3)";
        let tokenizer = RollTokenizer::new(expr);
        assert!(parse(tokenizer).is_err());

    }

    #[test]
    fn test_rpn_precedence() {
        let expr = "3+5d8*2";
        let tokenizer = RollTokenizer::new(expr);
        let parsed = parse(tokenizer).unwrap();

        let answer = vec![
            RPToken::Numeric(3),
            RPToken::Numeric(5),
            RPToken::Numeric(8),
            RPToken::DiceRollOperator,
            RPToken::Numeric(2),
            RPToken::Product,
            RPToken::Sum
        ];
        for (idx, tok) in parsed.iter().enumerate() {
            assert_eq!(answer[idx], *tok);
        }

    }

}