
mod parser;

fn main() {
    let expression = "3*(5d6 + 3d8)";
    let tokenizer = parser::RollTokenizer::new(expression);

    let rpn = parser::parse(tokenizer).unwrap();

    println!("RPN of expression: {:?}", rpn)
}


